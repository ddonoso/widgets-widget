package mall.wbd.widgets;

import mall.widgetti.helper.WidgetHelper;
import mall.widgetti.helper.WidgettiCache;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;
import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.CodeInsertionPosition;
import tooltwist.wbd.JavascriptCodeInserter;
import tooltwist.wbd.JavascriptLinkInserter;
import tooltwist.wbd.Snippet.SnippetLocation;
import tooltwist.wbd.SnippetParam;
import tooltwist.wbd.StylesheetCodeInserter;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;


public class SampleMap1 extends WbdWidgetController{
	 private static final String DEFAULT_SNIPPET_NAME = "sampleMap1";
		
	 @Override
	 protected void init(WbdWidget instance) throws WbdException {
	 }

	 @Override
	 public void renderForDesigner(WbdGenerator generator, WbdWidget instance, 
			 UimData ud, WbdRenderHelper rh) throws WbdException {
		 rh.append("Designer");
	}   
			

	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		 rh.append("Preview");
	}

	@Override
	public String getLabel(WbdWidget instance) throws WbdException {
		return "SampleMap1";
	}

	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		SnippetParam[] params={
				new SnippetParam("imagePathSantaMonica", WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathMacmallRetail",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathArrow",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathButtonClose",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathTorrance",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathHuntingBeach",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathMacmallRetailHB",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathChicago",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathMacmallRetailC",  WidgettiCache.getWebApp()+"/mall/mapImages/")
		};
		
		rh.renderSnippet(generator, instance, WidgetHelper.getHTMLFormatFileName(DEFAULT_SNIPPET_NAME), params);
			
	}
	
	
	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {	
		SnippetParam[] params={
				new SnippetParam("imagePathSantaMonica", WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathMacmallRetail",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathArrow",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathButtonClose",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathTorrance",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathHuntingBeach",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathMacmallRetailHB",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathChicago",  WidgettiCache.getWebApp()+"/mall/mapImages/"),
				new SnippetParam("imagePathMacmallRetailC",  WidgettiCache.getWebApp()+"/mall/mapImages/")
		};
		codeInserterList.add(new JavascriptLinkInserter("https://maps.googleapis.com/maps/api/js?key=AIzaSyDdpmKLqKeM61bUUOX4F3YoEdJluk85kb0&sensor=true",CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptLinkInserter("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js",CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance,SnippetLocation.THEMEABLE, WidgetHelper.getJavaScriptFormatFileName("infobox"), null, CodeInsertionPosition.HEADER));
		codeInserterList.add(new StylesheetCodeInserter(generator, instance,SnippetLocation.THEMEABLE, WidgetHelper.getCSSFormatFileName(DEFAULT_SNIPPET_NAME),null, CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance,SnippetLocation.THEMEABLE, WidgetHelper.getJavaScriptFormatFileName(DEFAULT_SNIPPET_NAME),params, CodeInsertionPosition.HEADER));
		
		
	}	
}
