package mall.wbd.widgets;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;

import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.CodeInsertionPosition;
import tooltwist.wbd.JavascriptCodeInserter;
import tooltwist.wbd.JavascriptLinkInserter;
import tooltwist.wbd.SnippetParam;
import tooltwist.wbd.StylesheetCodeInserter;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdStringProperty;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;
import tooltwist.wbd.Snippet.SnippetLocation;
import tooltwist.wbd.WidgetHelper;

public class CountDownTimer2 extends WbdWidgetController {

	 private static final String DEFAULT_SNIPPET_NAME = "countDownTimer2";
	
	
	@Override
	protected void init(WbdWidget instance) throws WbdException {
		instance.defineProperty(new WbdStringProperty("data-date", null, "", "2014-12-17 00:00:00"));	
	}

	@Override
	public void renderForDesigner(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		 rh.append("Designer");
		
	}

	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		 rh.append("Preview");
		
	}

	@Override
	public String getLabel(WbdWidget instance) throws WbdException {
		return "CountDownTimer2";
	}
	
	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		SnippetParam[] params = {new SnippetParam("data-date", instance.getFinalProperty(generator,"data-date"))}; 
		 rh.renderSnippet(generator, instance, WidgetHelper.getHTMLFormatFileName(DEFAULT_SNIPPET_NAME),params);

		
	}

	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {
		codeInserterList.add(new JavascriptLinkInserter("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", CodeInsertionPosition.HEADER));
		codeInserterList.add(new StylesheetCodeInserter(generator, instance, SnippetLocation.THEMEABLE, WidgetHelper.getCSSFormatFileName(DEFAULT_SNIPPET_NAME), null, CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance, SnippetLocation.THEMEABLE, WidgetHelper.getJavaScriptFormatFileName("timerCircles"), null, CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance, SnippetLocation.THEMEABLE, WidgetHelper.getJavaScriptFormatFileName(DEFAULT_SNIPPET_NAME), null, CodeInsertionPosition.HEADER));
		
		
	}
	public void render(WbdGenerator generator, WbdWidget instance, UimData ud,  WbdRenderHelper rh) throws WbdException{		
		rh.beforeProductionCode(generator, instance, null, true);
		rh.renderSnippetForProduction(generator, instance, WidgetHelper.getHTMLFormatFileName(DEFAULT_SNIPPET_NAME));
		rh.afterProductionCode(generator, instance);		
	}	 

}
