package mall.wbd.widgets;

import mall.widgetti.helper.WidgetHelper;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;

import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.CodeInsertionPosition;
import tooltwist.wbd.JavascriptCodeInserter;
import tooltwist.wbd.JavascriptLinkInserter;
import tooltwist.wbd.SnippetParam;
import tooltwist.wbd.StylesheetCodeInserter;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;
import tooltwist.wbd.Snippet.SnippetLocation;

public class CountDownTimer5 extends WbdWidgetController {

	private static final String DEFAULT_SNIPPET_NAME = "countDownTimer5";

	@Override
	protected void init(WbdWidget instance) throws WbdException {

	}

	@Override
	public void renderForDesigner(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		rh.append("Designer");

	}

	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		rh.append("Preview");

	}

	@Override
	public String getLabel(WbdWidget instance) throws WbdException {
		return "CountDownTimer5";
	}

	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		SnippetParam[] params = { new SnippetParam("futureDate",
				instance.getFinalProperty(generator, "futureDate")) };
		rh.renderSnippet(generator, instance,
				WidgetHelper.getHTMLFormatFileName(DEFAULT_SNIPPET_NAME),
				params);

	}

	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {
		codeInserterList
				.add(new JavascriptLinkInserter(
						"http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js",
						CodeInsertionPosition.HEADER));
		codeInserterList.add(new StylesheetCodeInserter(generator, instance,
				SnippetLocation.THEMEABLE, WidgetHelper
						.getCSSFormatFileName(DEFAULT_SNIPPET_NAME), null,
				CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance,
				SnippetLocation.THEMEABLE, WidgetHelper
						.getJavaScriptFormatFileName(DEFAULT_SNIPPET_NAME),
				null, CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance,
				SnippetLocation.THEMEABLE, WidgetHelper
						.getJavaScriptFormatFileName("base"), null,
				CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance,
				SnippetLocation.THEMEABLE, WidgetHelper
						.getJavaScriptFormatFileName("flipclock.min"), null,
				CodeInsertionPosition.HEADER));

		codeInserterList.add(new JavascriptCodeInserter(generator, instance,
				SnippetLocation.THEMEABLE, WidgetHelper
						.getJavaScriptFormatFileName("dailycounter"), null,
				CodeInsertionPosition.HEADER));

	}

	public void render(WbdGenerator generator, WbdWidget instance, UimData ud,
			WbdRenderHelper rh) throws WbdException {
		rh.beforeProductionCode(generator, instance, null, true);
		rh.renderSnippetForProduction(generator, instance,
				WidgetHelper.getHTMLFormatFileName(DEFAULT_SNIPPET_NAME));
		rh.afterProductionCode(generator, instance);
	}

}
