
var style_macmallRetail = [
  {
    "featureType": "administrative",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "poi",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "transit",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "landscape",
    "stylers": [
      { "color": "#FFE200" }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "visibility": "on" },
      { "color": "#4f92c6" }
    ]
  }
];

var style_macmallRetail_zoomed = [
  {
    "featureType": "administrative",
    "stylers": [
      { "visibility": "on" }
    ]
  },{
    "featureType": "poi",
    "stylers": [
      { "visibility": "on" }
    ]
  },{
    "featureType": "transit",
    "stylers": [
      { "visibility": "on" }
    ]
  },{
    "featureType": "road",
    "stylers": [
      { "visibility": "on" }
    ]
  },{
    "featureType": "landscape",
    "stylers": [
      { "color": "#FFE200" }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "visibility": "on" },
      { "color": "#4f92c6" }
    ]
  },   {
    "featureType": "poi.park",
	"elementType": "geometry",
    "stylers": [
      { "color": "#FFFF00" }
    ]
  }
];

var styled_macmallRetail = new google.maps.StyledMapType(style_macmallRetail, {name: "MacmallRetail style"});
var styled_macmallRetail_zoomed = new google.maps.StyledMapType(style_macmallRetail_zoomed, {name: "MacmallRetail style zoomed"});

//center of the map.
var macmallRetailMapCenter = new google.maps.LatLng(34.027811, -118.487850);
var macmallRetailMapZoom = 3;
var macmallRetailMapZoomMax = 30;
var macmallRetailMapZoomMin = 3;

//These options configure the setup of the map. 
var macmallRetailMapOptions = { 
		  center: macmallRetailMapCenter, 
          zoom: macmallRetailMapZoom,
		  maxZoom:macmallRetailMapZoomMax,
		  minZoom:macmallRetailMapZoomMin,
		  panControl: false,
		  mapTypeControl: false,
		  mapTypeControlOptions: {
     	  mapTypeIds: [google.maps.MapTypeId.SATELLITE,'map_styles_macmallRetail', 'map_styles_macmallRetail_zoomed']
   		 }
};

var macmallRetailMap;
var controlPanelDiv = document.createElement('div');
var resetButtonDiv = document.createElement('div');
var macmallRetailMapResetButton = new createResetButton(resetButtonDiv, macmallRetailMap);
var pop_up_info = "border: 0px solid black; background-color: #ffffff; padding:15px; margin-top: 8px; border-radius:10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; box-shadow: 1px 1px #888;";

google.maps.event.addDomListener(window, 'load', loadMacmallRetailMap);

function loadMacmallRetailMap() {
	macmallRetailMap = new google.maps.Map(document.getElementById("macmallRetail-map"), macmallRetailMapOptions);	
	macmallRetailMap.mapTypes.set('map_styles_macmallRetail', styled_macmallRetail);
	macmallRetailMap.mapTypes.set('map_styles_macmallRetail_zoomed', styled_macmallRetail_zoomed);
	macmallRetailMap.setMapTypeId('map_styles_macmallRetail');


	google.maps.event.addListener(macmallRetailMap, "zoom_changed", function() {
		var newZoom = macmallRetailMap.getZoom();
		if (newZoom > 6){
			macmallRetailMap.setMapTypeId('map_styles_macmallRetail_zoomed');
				}
		else {
			macmallRetailMap.setMapTypeId('map_styles_macmallRetail');
		}

});


macmallRetailMap.controls[google.maps.ControlPosition.RIGHT_TOP].push(controlPanelDiv);
macmallRetailMap.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(resetButtonDiv);

loadMapMarkers();

}



//Function that loads the map markers and the pop-up info boxes.
function loadMapMarkers (){

	//SantaMonica 
	var markerPositionSantaMonica = new google.maps.LatLng(34.027811, -118.487850);
	var imagePathSantaMonica = '%%imagePathSantaMonica%%macmall_mapDefault.png';
	var markerIconSantaMonica = {
	  url: imagePathSantaMonica,
	  size: new google.maps.Size(225, 120),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(189, 116)
	};
	
	var markerShapeSantaMonica = {
	      coord: [12,4,216,22,212,74,157,70,184,111,125,67,6,56],
	      type: 'poly'
	};
	
	
	markerSantaMonica = new google.maps.Marker({
		  position: markerPositionSantaMonica,
	      map: macmallRetailMap,
	      title: 'Santa Monica Store',
		  icon: markerIconSantaMonica,
		  shape: markerShapeSantaMonica,
		  zIndex:107
	});
	

	var boxTextSantaMonica = document.createElement("div");
	boxTextSantaMonica.style.cssText = pop_up_info;
	boxTextSantaMonica.innerHTML = '<span class="pop_up_box_text"><img src="%%imagePathMacmallRetail%%macmallretailDefault.png" width="400" height="285" border="0" /></span>';
            
	var infoboxOptionsSantaMonica = {
	                 content: boxTextSantaMonica
	                ,disableAutoPan: false
	                ,maxWidth: 0
	                ,pixelOffset: new google.maps.Size(-241, 0)
	                ,zIndex: null
	                ,boxStyle: { 
	                  background: "url('%%imagePathArrow%%pop_up_box_top_arrow.png') no-repeat"
	                  ,opacity: 1
	                  ,width: "430px"
	                 }
	                ,closeBoxMargin: "10px 2px 2px 2px"
	                ,closeBoxURL: "%%imagePathButtonClose%%button_close.png"
	                ,infoBoxClearance: new google.maps.Size(1, 1)
	                ,isHidden: false
	                ,pane: "floatPane"
	                ,enableEventPropagation: false
	};
	

	infoboxSantaMonica = new InfoBox(infoboxOptionsSantaMonica);
	
	
	google.maps.event.addListener(markerSantaMonica, "click", function (e) {
				infoboxSantaMonica.open(macmallRetailMap, this);
				this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
				setZoomWhenMarkerClicked();
				macmallRetailMmap.setCenter(markerSantaMonica.getPosition());
	});
	
	
	//Torrance
	var markerPositionTorrance = new google.maps.LatLng(33.821421, -118.351327);
	var imageTorrance = '%%imagePathTorrance%%macmall_mapTorrance.png';
	var markerIconTorrance = {
	  url: imageTorrance,
	  size: new google.maps.Size(300, 200),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(300, 50)
	};
	
	var markerShapeIsleOfWight = {
	      coord: [6,30,180,4,192,70,103,82,105,105,82,85,15,93],
	      type: 'poly'
	
	};
	
	markerTorrance = new google.maps.Marker({
		  position: markerPositionTorrance,
	      map: macmallRetailMap,
	      title: 'Torrance Store',
		  icon: markerIconTorrance,
		  shape: markerShapeIsleOfWight,
		  zIndex:108
	});
	var boxTextTorrance = document.createElement("div");
	boxTextTorrance.style.cssText = pop_up_info;
	boxTextTorrance.innerHTML = '<span class="pop_up_box_text"><img src="%%imagePathMacmallRetail%%macmallretailDefault.png" width="400" height="285" border="0" /></span>';
	         
	var infoboxOptionsTorrance = {
	                 content: boxTextTorrance
	                ,disableAutoPan: false
	                ,maxWidth: 0
	                ,pixelOffset: new google.maps.Size(-241, 0)
	                ,zIndex: null
	                ,boxStyle: { 
	                  background: "url('%%imagePathArrow%%pop_up_box_top_arrow.png') no-repeat"
	                  ,opacity: 1
	                  ,width: "430px"
	                 }
	                ,closeBoxMargin: "10px 2px 2px 2px"
	                ,closeBoxURL: "%%imagePathButtonClose%%button_close.png"
	                ,infoBoxClearance: new google.maps.Size(1, 1)
	                ,isHidden: false
	                ,pane: "floatPane"
	                ,enableEventPropagation: false
	};

	infoboxTorrance = new InfoBox(infoboxOptionsTorrance);

	google.maps.event.addListener(markerTorrance, "click", function (e) {
				infoboxTorrance.open(macmallRetailMap, this);
				this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
				setZoomWhenMarkerClicked();
				macmallRetailMmap.setCenter(markerTorrance.getPosition());
	});
	

	
	//Huntington Beach
	var markerPositionHuntingtonBeach = new google.maps.LatLng(33.716770, -117.989828);
	var imageHuntingBeach = '%%imagePathHuntingBeach%%macmall_mapHuntingTonBeach.png';
	var markerIconHuntingtonBeach = {
	  url: imageHuntingBeach,
	  size: new google.maps.Size(216, 151),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(25, 12)
	};
	
	var markerShapeHuntingtonBeach = {
	      coord: [18,8,208,28,200,113,162,110,190,145,128,109,6,93],
	      type: 'poly'
	};
	
	markerHuntingtonBeach = new google.maps.Marker({
		  position: markerPositionHuntingtonBeach,
	      map: macmallRetailMap,
	      title: 'Huntington Beach Store',
		  icon: markerIconHuntingtonBeach,
		  shape: markerShapeHuntingtonBeach,
		  zIndex:103
	});
	
	
	var boxTextHuntingtonBeach = document.createElement("div");
	boxTextHuntingtonBeach.style.cssText = pop_up_info;
	boxTextHuntingtonBeach.innerHTML = '<span class="pop_up_box_text"><img src="%%imagePathMacmallRetailHB%%macmallretailHuntingtonBeach.png" width="400" height="285" border="0" /></span>';
	
	   
	var infoboxOptionsHuntingtonBeach = {
	                 content: boxTextHuntingtonBeach
	                ,disableAutoPan: false
	                ,maxWidth: 0
	                ,pixelOffset: new google.maps.Size(-241, 0)
	                ,zIndex: null
	                ,boxStyle: { 
	                  background: "url('%%imagePathArrow%%pop_up_box_top_arrow.png') no-repeat"
	                  ,opacity: 1
	                  ,width: "430px"
	                 }
	                ,closeBoxMargin: "10px 2px 2px 2px"
	                ,closeBoxURL: "%%imagePathButtonClose%%button_close.png"
	                ,infoBoxClearance: new google.maps.Size(1, 1)
	                ,isHidden: false
	                ,pane: "floatPane"
	                ,enableEventPropagation: false
	};
	
	infoboxHuntingtonBeach = new InfoBox(infoboxOptionsHuntingtonBeach);
	
	
	google.maps.event.addListener(markerHuntingtonBeach, "click", function (e) {		
				infoboxHuntingtonBeach.open(macmallRetailMap, this);			
				this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);			
				setZoomWhenMarkerClicked();		
				macmallRetailMmap.setCenter(markerHuntingtonBeach.getPosition());
	});
	

	//Chicago
	var markerPositionChicago = new google.maps.LatLng(41.891442, -87.636719);
	var imageChicago = '%%imagePathChicago%%macmall_mapChicago.png';
	var markerIconChicago = {
	  url: imageChicago,
	  size: new google.maps.Size(196, 114),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(35, 90)
	};
	
	var markerShapeChicago = {
	      coord: [8,54,177,7,189,49,65,88,44,110,47,91,20,98],
	      type: 'poly'
	};
	
	markerChicago = new google.maps.Marker({
		  position: markerPositionChicago,
	      map: macmallRetailMap,
	      title: 'Chicago Store',
		  icon: markerIconChicago,
		  shape: markerShapeChicago,
		  zIndex:106
	});

	var boxTextChicago = document.createElement("div");
	boxTextChicago.style.cssText = pop_up_info;
	boxTextChicago.innerHTML = '<span class="pop_up_box_text"><img src="%%imagePathMacmallRetailC%%macmallretailChicago.png" width="400" height="285" border="0" /></span>';
	            
	var infoboxOptionsChicago = {
	                 content: boxTextChicago
	                ,disableAutoPan: false
	                ,maxWidth: 0
	                ,pixelOffset: new google.maps.Size(-241, 0)
	                ,zIndex: null
	                ,boxStyle: { 
	                  background: "url('%%imagePathArrow%%pop_up_box_top_arrow.png') no-repeat"
	                  ,opacity: 1
	                  ,width: "430px"
	                 }
	                ,closeBoxMargin: "10px 2px 2px 2px"
	                ,closeBoxURL: "%%imagePathButtonClose%%button_close.png"
	                ,infoBoxClearance: new google.maps.Size(1, 1)
	                ,isHidden: false
	                ,pane: "floatPane"
	                ,enableEventPropagation: false
	};
	

	infoboxChicago = new InfoBox(infoboxOptionsChicago);
	

	google.maps.event.addListener(markerChicago, "click", function (e) {
				infoboxChicago.open(macmallRetailMap, this);
				this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
				setZoomWhenMarkerClicked();
				macmallRetailMmap.setCenter(markerChicago.getPosition());
	});


}

function setZoomWhenMarkerClicked(){
var currentZoom = macmallRetailMap.getZoom();
	if (currentZoom < 7){
			macmallRetailMap.setZoom(7);
	}
}

function resetZindexes (){
	markerHuntingtonBeach.setZIndex(103);	
	markerChicago.setZIndex(106);
	markerSantaMonica.setZIndex(107);
	markerTorrance.setZIndex(108);
}

function createControlPanel (controlPanelDiv){
   controlPanelDiv.appendChild(controlUI); 
}


function createResetButton (resetButtonDiv){
  resetButtonDiv.style.padding = '0px';  
  controlUI2 = document.createElement('div');
  controlUI2.style.borderRadius='5px';
  controlUI2.style.margin='10px';
  controlUI2.style.paddingTop='2px';
  controlUI2.style.paddingBottom='2px';
  controlUI2.style.paddingLeft='2px';
  controlUI2.style.paddingRight='5px';
  controlUI2.style.textAlign='center';
  controlUI2.style.width='148px';
  controlUI2.style.height='31px';
  resetButtonDiv.appendChild(controlUI2);
}


function closeAllInfoboxes(){
	infoboxSantaMonica.close();
	infoboxTorrance.close();
	infoboxHuntingtonBeach.close();
	infoboxChicago.close();
}