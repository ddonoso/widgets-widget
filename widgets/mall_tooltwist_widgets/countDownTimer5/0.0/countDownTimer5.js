var clock;

$(document).ready(function() {

	var currentDate = new Date();
	var futureDate = new Date("December 31 2013 01:15:00");
	var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

	clock = $('.clock').FlipClock(diff, {
		clockFace : 'DailyCounter',
		countdown : true
	});
});